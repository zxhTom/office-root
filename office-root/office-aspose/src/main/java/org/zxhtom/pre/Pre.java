package org.zxhtom.pre;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Pre {
	private static Logger logger = LogManager.getLogger(Pre.class);
	private static  Pre pre;
	private Pre(){}
	public static Pre getInstance(){
		if(null==pre){
			pre = new Pre();
		}
		return pre;
	}
	
	public void Hello(){
		logger.info("testsett");
		System.out.println("Hello World");
	}
}
