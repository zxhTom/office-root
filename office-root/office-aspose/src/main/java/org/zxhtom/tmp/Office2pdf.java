package org.zxhtom.tmp;

import java.io.File;
import java.io.FileOutputStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zxhtom.base.LicenseFactory;

import com.aspose.cells.Workbook;
import com.aspose.words.Document;

public class Office2pdf {
	private static Logger logger = LogManager.getLogger(Office2pdf.class);
	public static void doc2pdf(String wordPath, String pdfPath) {
        if (!LicenseFactory.getInstance().isPassLicense(com.aspose.words.License.class)) {          // 验证License 若不验证则转化出的pdf文档会有水印产生
            return;
        }
        try {
            long old = System.currentTimeMillis();
            File file = new File(pdfPath);  //新建一个pdf文档
            FileOutputStream os = new FileOutputStream(file);
            Document doc = new Document(wordPath);                    //Address是将要被转化的word文档
            doc.save(os, com.aspose.words.SaveFormat.PDF);//全面支持DOC, DOCX, OOXML, RTF HTML, OpenDocument, PDF, EPUB, XPS, SWF 相互转换
            long now = System.currentTimeMillis();
            os.close();
            System.out.println("共耗时：" + ((now - old) / 1000.0) + "秒");  //转化用时
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param excelPath
     * @param pdfPath
     */
    public static void excel2pdf(String excelPath, String pdfPath) {
    	if (!LicenseFactory.getInstance().isPassLicense(com.aspose.cells.License.class)) {          // 验证License 若不验证则转化出的pdf文档会有水印产生
            return;
        }
        try {
            long old = System.currentTimeMillis();
            Workbook wb = new Workbook(excelPath);// 原始excel路径
            FileOutputStream fileOS = new FileOutputStream(new File(pdfPath));
            wb.save(fileOS, com.aspose.cells.SaveFormat.PDF);
            fileOS.close();
            long now = System.currentTimeMillis();
            System.out.println("共耗时：" + ((now - old) / 1000.0) + "秒");  //转化用时
        } catch (Exception e) {
            logger.error("报错："+e);
        }
    }
    public static void test(){
    	System.out.println("201802031516test");
    }
    public static void main(String[] args) {
    	String source = "E://zxh//test//tew.xls";
		String target = "E://zxh//test//tew.pdf";
		excel2pdf(source, target);
	}
}
