package org.zxhtom.base.saveformat.words;

public class SaveFormat {
	public static final int UNKNOWN = 0;
	public static final int DOC = 10;
	public static final int DOT = 11;
	public static final int DOCX = 20;
	public static final int DOCM = 21;
	public static final int DOTX = 22;
	public static final int DOTM = 23;
	public static final int FLAT_OPC = 24;
	public static final int FLAT_OPC_MACRO_ENABLED = 25;
	public static final int FLAT_OPC_TEMPLATE = 26;
	public static final int FLAT_OPC_TEMPLATE_MACRO_ENABLED = 27;
	public static final int RTF = 30;
	public static final int WORD_ML = 31;
	public static final int PDF = 40;
	public static final int XPS = 41;
	public static final int XAML_FIXED = 42;
	public static final int SWF = 43;
	public static final int SVG = 44;
	public static final int HTML_FIXED = 45;
	public static final int OPEN_XPS = 46;
	public static final int PS = 47;
	public static final int HTML = 50;
	public static final int MHTML = 51;
	public static final int EPUB = 52;
	public static final int ODT = 60;
	public static final int OTT = 61;
	public static final int TXT = 70;
	public static final int XAML_FLOW = 71;
	public static final int XAML_FLOW_PACK = 72;
	public static final int TIFF = 100;
	public static final int PNG = 101;
	public static final int BMP = 102;
	public static final int EMF = 103;
	public static final int JPEG = 104;
	public static final int GIF = 105;
	public static final int length = 35;

	public static String getName(int saveFormat) {
		switch (saveFormat) {
		case 0:
			return "UNKNOWN";
		case 10:
			return "DOC";
		case 11:
			return "DOT";
		case 20:
			return "DOCX";
		case 21:
			return "DOCM";
		case 22:
			return "DOTX";
		case 23:
			return "DOTM";
		case 24:
			return "FLAT_OPC";
		case 25:
			return "FLAT_OPC_MACRO_ENABLED";
		case 26:
			return "FLAT_OPC_TEMPLATE";
		case 27:
			return "FLAT_OPC_TEMPLATE_MACRO_ENABLED";
		case 30:
			return "RTF";
		case 31:
			return "WORD_ML";
		case 40:
			return "PDF";
		case 41:
			return "XPS";
		case 42:
			return "XAML_FIXED";
		case 43:
			return "SWF";
		case 44:
			return "SVG";
		case 45:
			return "HTML_FIXED";
		case 46:
			return "OPEN_XPS";
		case 47:
			return "PS";
		case 50:
			return "HTML";
		case 51:
			return "MHTML";
		case 52:
			return "EPUB";
		case 60:
			return "ODT";
		case 61:
			return "OTT";
		case 70:
			return "TEXT";
		case 71:
			return "XAML_FLOW";
		case 72:
			return "XAML_FLOW_PACK";
		case 100:
			return "TIFF";
		case 101:
			return "PNG";
		case 102:
			return "BMP";
		case 103:
			return "EMF";
		case 104:
			return "JPEG";
		case 105:
			return "GIF";
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:
		case 7:
		case 8:
		case 9:
		case 12:
		case 13:
		case 14:
		case 15:
		case 16:
		case 17:
		case 18:
		case 19:
		case 28:
		case 29:
		case 32:
		case 33:
		case 34:
		case 35:
		case 36:
		case 37:
		case 38:
		case 39:
		case 48:
		case 49:
		case 53:
		case 54:
		case 55:
		case 56:
		case 57:
		case 58:
		case 59:
		case 62:
		case 63:
		case 64:
		case 65:
		case 66:
		case 67:
		case 68:
		case 69:
		case 73:
		case 74:
		case 75:
		case 76:
		case 77:
		case 78:
		case 79:
		case 80:
		case 81:
		case 82:
		case 83:
		case 84:
		case 85:
		case 86:
		case 87:
		case 88:
		case 89:
		case 90:
		case 91:
		case 92:
		case 93:
		case 94:
		case 95:
		case 96:
		case 97:
		case 98:
		case 99:
		}
		return "Unknown SaveFormat value.";
	}

	public static String toString(int saveFormat) {
		switch (saveFormat) {
		case 0:
			return "Unknown";
		case 10:
			return "Doc";
		case 11:
			return "Dot";
		case 20:
			return "Docx";
		case 21:
			return "Docm";
		case 22:
			return "Dotx";
		case 23:
			return "Dotm";
		case 24:
			return "FlatOpc";
		case 25:
			return "FlatOpcMacroEnabled";
		case 26:
			return "FlatOpcTemplate";
		case 27:
			return "FlatOpcTemplateMacroEnabled";
		case 30:
			return "Rtf";
		case 31:
			return "WordML";
		case 40:
			return "Pdf";
		case 41:
			return "Xps";
		case 42:
			return "XamlFixed";
		case 43:
			return "Swf";
		case 44:
			return "Svg";
		case 45:
			return "HtmlFixed";
		case 46:
			return "OpenXps";
		case 47:
			return "Ps";
		case 50:
			return "Html";
		case 51:
			return "Mhtml";
		case 52:
			return "Epub";
		case 60:
			return "Odt";
		case 61:
			return "Ott";
		case 70:
			return "Text";
		case 71:
			return "XamlFlow";
		case 72:
			return "XamlFlowPack";
		case 100:
			return "Tiff";
		case 101:
			return "Png";
		case 102:
			return "Bmp";
		case 103:
			return "Emf";
		case 104:
			return "Jpeg";
		case 105:
			return "Gif";
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:
		case 7:
		case 8:
		case 9:
		case 12:
		case 13:
		case 14:
		case 15:
		case 16:
		case 17:
		case 18:
		case 19:
		case 28:
		case 29:
		case 32:
		case 33:
		case 34:
		case 35:
		case 36:
		case 37:
		case 38:
		case 39:
		case 48:
		case 49:
		case 53:
		case 54:
		case 55:
		case 56:
		case 57:
		case 58:
		case 59:
		case 62:
		case 63:
		case 64:
		case 65:
		case 66:
		case 67:
		case 68:
		case 69:
		case 73:
		case 74:
		case 75:
		case 76:
		case 77:
		case 78:
		case 79:
		case 80:
		case 81:
		case 82:
		case 83:
		case 84:
		case 85:
		case 86:
		case 87:
		case 88:
		case 89:
		case 90:
		case 91:
		case 92:
		case 93:
		case 94:
		case 95:
		case 96:
		case 97:
		case 98:
		case 99:
		}
		return "Unknown SaveFormat value.";
	}

	public static int fromName(String saveFormatName) {
		if ("UNKNOWN".equals(saveFormatName))
			return 0;
		if ("DOC".equals(saveFormatName))
			return 10;
		if ("DOT".equals(saveFormatName))
			return 11;
		if ("DOCX".equals(saveFormatName))
			return 20;
		if ("DOCM".equals(saveFormatName))
			return 21;
		if ("DOTX".equals(saveFormatName))
			return 22;
		if ("DOTM".equals(saveFormatName))
			return 23;
		if ("FLAT_OPC".equals(saveFormatName))
			return 24;
		if ("FLAT_OPC_MACRO_ENABLED".equals(saveFormatName))
			return 25;
		if ("FLAT_OPC_TEMPLATE".equals(saveFormatName))
			return 26;
		if ("FLAT_OPC_TEMPLATE_MACRO_ENABLED".equals(saveFormatName))
			return 27;
		if ("RTF".equals(saveFormatName))
			return 30;
		if ("WORD_ML".equals(saveFormatName))
			return 31;
		if ("PDF".equals(saveFormatName))
			return 40;
		if ("XPS".equals(saveFormatName))
			return 41;
		if ("XAML_FIXED".equals(saveFormatName))
			return 42;
		if ("SWF".equals(saveFormatName))
			return 43;
		if ("SVG".equals(saveFormatName))
			return 44;
		if ("HTML_FIXED".equals(saveFormatName))
			return 45;
		if ("OPEN_XPS".equals(saveFormatName))
			return 46;
		if ("PS".equals(saveFormatName))
			return 47;
		if ("HTML".equals(saveFormatName))
			return 50;
		if ("MHTML".equals(saveFormatName))
			return 51;
		if ("EPUB".equals(saveFormatName))
			return 52;
		if ("ODT".equals(saveFormatName))
			return 60;
		if ("OTT".equals(saveFormatName))
			return 61;
		if ("TEXT".equals(saveFormatName))
			return 70;
		if ("XAML_FLOW".equals(saveFormatName))
			return 71;
		if ("XAML_FLOW_PACK".equals(saveFormatName))
			return 72;
		if ("TIFF".equals(saveFormatName))
			return 100;
		if ("PNG".equals(saveFormatName))
			return 101;
		if ("BMP".equals(saveFormatName))
			return 102;
		if ("EMF".equals(saveFormatName))
			return 103;
		if ("JPEG".equals(saveFormatName))
			return 104;
		if ("GIF".equals(saveFormatName))
			return 105;
		throw new IllegalArgumentException("Unknown SaveFormat name.");
	}

	public static int[] getValues() {
		return new int[] { 0, 10, 11, 20, 21, 22, 23, 24, 25, 26, 27, 30, 31, 40, 41, 42, 43, 44, 45, 46, 47, 50, 51, 52, 60, 61, 70, 71, 72, 100, 101, 102, 103, 104, 105 };
	}
}