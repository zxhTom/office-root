package org.zxhtom.base.saveformat.slides;



public final class SaveFormat
{
  public static final int PPT = 0;
  public static final int PPS = 0;
  public static final int PDF = 1;
  public static final int XPS = 2;
  public static final int PPTX = 3;
  public static final int PPSX = 4;
  public static final int TIFF = 5;
  public static final int ODP = 6;
  public static final int PPTM = 7;
  public static final int PPSM = 9;
  public static final int POTX = 10;
  public static final int POTM = 11;
  public static final int PDFNOTES = 12;
  public static final int HTML = 13;
  public static final int TIFFNOTES = 14;

}
