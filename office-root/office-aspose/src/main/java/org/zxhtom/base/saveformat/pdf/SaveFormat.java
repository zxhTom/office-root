package org.zxhtom.base.saveformat.pdf;

public class SaveFormat {

	public static final int PDF = 0;
	public static final int NONE = 0;
	public static final int DOC = 1;
	public static final int XPS = 2;
	public static final int HTML = 3;
	public static final int XML = 4;
	public static final int TEX = 5;
	public static final int DOCX = 6;
	public static final int SVG = 7;
	public static final int MOBIXML = 8;
	public static final int EXCEL = 9;
	public static final int EPUB = 10;
	public static final int PLUGIN = 11;
	public static final int PPTX = 14;
}
