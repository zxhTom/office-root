package org.zxhtom.base.saveformat.cells;

public class SaveFormat
{
  public static final int CSV = 1;
  public static final int XLSX = 6;
  public static final int XLSM = 7;
  public static final int XLTX = 8;
  public static final int XLTM = 9;
  public static final int XLAM = 10;
  public static final int TAB_DELIMITED = 11;
  public static final int HTML = 12;
  public static final int M_HTML = 17;
  public static final int ODS = 14;
  public static final int EXCEL_97_TO_2003 = 5;
  public static final int SPREADSHEET_ML = 15;
  public static final int XLSB = 16;
  public static final int AUTO = 0;
  public static final int UNKNOWN = 255;
  public static final int PDF = 13;
  public static final int XPS = 20;
  public static final int TIFF = 21;
  public static final int SVG = 22;
  public static final int DIF = 30;
}
