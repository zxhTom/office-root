package org.zxhtom.base;

import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;








import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zxhtom.Exception.CommonException;

import com.aspose.pdf.License;


/**
 * 证书验证
 * 
 * @author zxh
 * 
 */
public class LicenseFactory {

	private Logger logger = LogManager.getLogger();
	private static LicenseFactory licenseFactory;

	private LicenseFactory() {
	}

	public static LicenseFactory getInstance() {
		if (null == licenseFactory) {
			licenseFactory = new LicenseFactory();
		}
		return licenseFactory;
	}
	
	public boolean isPassLicense(Class<?> clazz){
		try {
			/**读取resource文件下的验证信息*/
			InputStream is = LicenseFactory.class.getClassLoader().getResourceAsStream("license.xml");
			Method method = clazz.getMethod("setLicense", InputStream.class);
			method.invoke(clazz.newInstance(), is);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error(e.getTargetException() + String.format(" : %1$s is not avalid ", clazz.getName()));
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}
}
