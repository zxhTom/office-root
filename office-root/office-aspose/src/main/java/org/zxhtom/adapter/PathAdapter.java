package org.zxhtom.adapter;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zxhtom.Exception.CommonException;
import org.zxhtom.utils.ReflectUtil;

/**
 * Description: 根据路径转换成对应的操作类
 * @ClassName: PathAdapter
 * @author zxhtom
 * @date 2018年1月29日
 */
public class PathAdapter {

	private Logger logger = LogManager.getLogger(PathAdapter.class);
	private String sourceClassPath = "";
	private String key="";
	private String license="";
	private static Map<String, Object> paramMap = new HashMap<String, Object>();
	private static Map<String, Object> targetMap = new HashMap<String, Object>();
	private static String formatTemplete="org.zxhtom.base.saveformat.%1$s.SaveFormat";
	private static String keyTemplete = "com.aspose.%1$s.License";
	/**
	 * 目前匹配项如下
	 * doc|docx : word
	 * xls|xlsx : excel
	 */
	private static String pathRegex ;
	static{
		pathRegex = "^.*\\.(doc|xls|ppt|pdf|html|txt)x?$";
		/**将office中的文档与aspose中jar路径对接*/
		paramMap.put("doc", "com.aspose.words.Document");
		paramMap.put("xls", "com.aspose.cells.Workbook");
		paramMap.put("ppt", "com.aspose.slides.Presentation");
		paramMap.put("pdf", "com.aspose.pdf.Document");
		/**office格式与aspose类对接*/
		/*targetMap.put("pdf_words", com.aspose.words.SaveFormat.PDF);
		targetMap.put("pdf_cells", com.aspose.cells.SaveFormat.PDF);
		targetMap.put("pdf_slides", com.aspose.slides.SaveFormat.Pdf);*/
	}
	private void setKey(String source) throws CommonException{
		/**通过原文件类路径匹配关键词*/
		String regex = "^.*\\..*\\.(.*)\\..*";
		Matcher matcher = Pattern.compile(regex).matcher(sourceClassPath);
		if(matcher.find()){
			key=matcher.group(1);
			license=String.format(keyTemplete, key);
		}else {
			throw new CommonException("not find keys");
		}
	}
	public String getLicensePath(String source) throws CommonException{
		return license;
	}
	public Class<?> getClassByPath(String source) throws ClassNotFoundException, CommonException {
		Matcher matcher = Pattern.compile(pathRegex).matcher(source);
		if(matcher.find()){
			Object sourceObj = paramMap.get(matcher.group(1));
			if(null==sourceObj){
				throw new CommonException(String.format("not register the type of %1$s Handler,please contact q:870775401 help", matcher.group(1)));
			}
			sourceClassPath = sourceObj.toString();
			setKey(source);
			return Class.forName(sourceClassPath);
		}else {
			logger.warn(String.format("source (%1$s) is not avalid ", source));
		}
		return Object.class;
	}
	
	public int getFormatByPathTmp(String target) throws CommonException{
		if(StringUtils.isBlank(sourceClassPath)){
			throw new CommonException("You must first call the getClassByPath method");
		}
		Matcher matcherTarget = Pattern.compile(pathRegex).matcher(target);
		if(matcherTarget.find()){
			return (Integer) targetMap.get(matcherTarget.group(1)+"_"+key);
		}else {
			throw new CommonException("target unlawful");
		}
	}
	/**
	 * 
	 * Description: 根据目标路径动态转换，目前由于无法实例final类。此方法弃用
	 * @param target
	 * @return
	 * @throws CommonException
	 * @throws ClassNotFoundException   
	 * @throws
	 * @author zxhtom
	 * @date 2018年1月29日
	 */
	public int getFormatByPath(String target) throws CommonException, ClassNotFoundException{
		if(StringUtils.isBlank(sourceClassPath)){
			throw new CommonException("You must first call the getClassByPath method");
		}
		Matcher matcherTarget = Pattern.compile(pathRegex).matcher(target);
		if(matcherTarget.find()){
			String format = String.format(formatTemplete, key);
			return (Integer) ReflectUtil.getField(Class.forName(format), matcherTarget.group(1).toUpperCase());
		}else {
			throw new CommonException("target unlawful");
		}
	}

}
