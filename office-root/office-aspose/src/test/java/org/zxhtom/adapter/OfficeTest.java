package org.zxhtom.adapter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Test;
import org.zxhtom.service.TransformOffice;

public class OfficeTest {

	//@Test
	public void transForm() throws IOException {

		String source = "E://zxh//test//lqj";
		String target = "E://zxh//test//tew.pdf";
		TransformOffice trans = new TransformOffice();
		// trans.doTransform(source, target);
		File pptFile = new File(source + "//ppt");
		File docFile = new File(source + "//doc");
		if (!pptFile.exists()) {
			pptFile.mkdirs();
		}
		if (!docFile.exists()) {
			docFile.mkdirs();
		}
		ArrayList<String> files = getFiles(source);
		for (String file : files) {
			String fileName = "lqj";
			fileName = getFileName(file);
			String pathName = source;
			if (file.indexOf(".ppt") != -1) {
				pathName += "//ppt//" + fileName;
			} else if (file.indexOf(".doc") != -1) {
				pathName += "//doc//" + fileName;
			} else {
				continue;
			}
			trans.doTransform(file, pathName + ".pdf");
		}
		System.out.println("complete");
	}

	public String getFileName(String filePath) {
		String reg = "([^<>/\\\\|:\"\"\\*\\?]+)\\.\\w+$+";
		Matcher m = Pattern.compile(reg).matcher(filePath); // uri为需要匹配的路径
		String filename = null;
		if (m.find()) {
			filename = m.group(1);
		}
		return filename;
	}

	public static ArrayList<String> getFiles(String path) {
		ArrayList<String> files = new ArrayList<String>();
		File file = new File(path);
		File[] tempList = file.listFiles();

		for (int i = 0; i < tempList.length; i++) {
			if (tempList[i].isFile()) {
				// System.out.println("文     件：" + tempList[i]);
				files.add(tempList[i].toString());
			}
			if (tempList[i].isDirectory()) {
				// System.out.println("文件夹：" + tempList[i]);
			}
		}
		return files;
	}

	public static void main(String[] args) throws ReflectiveOperationException, SecurityException {
		String source = "E://zxh//test//123.doc";
		String target = "E://zxh//test//test2_1.pdf";
		TransformOffice trans = new TransformOffice();
		trans.doTransform(source, target);
	}
}
