package org.office.poi.base;

/**
 * 
 * 简介：操作的Excel表格的版本，目前2003和其他版本不一样，所以这里做区分<BR/>
 *
 * 描述：枚举Excel版本<BR/>
 *
 * @author  xinhua
 * 
 * @since JDK1.7
 *
 * @version  V1.00
 *
 * @date 2017年3月22日上午10:55:53
 */
public enum ExcelType {
    
    /*2003*/
	EXCEL2003,
	
	/*2007或更高*/
	EXCEL2007;
}
