package org.office.poi;

import org.office.poi.base.OnReadDataHandler;
import org.office.poi.expection.ExcelException;
import org.office.poi.util.ExcelTool;

import java.io.File;
import java.util.List;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    public static void main(String[] args) throws ExcelException {
    	File file  = new File("/root/Documents/test.xlsx");
        ExcelTool.$import(true,true).toData(file, new OnReadDataHandler()
        {
            
            @Override
            public void handler(List<Object> rowData)
            {
                System.out.println(rowData);
            }
        });
	}
}
