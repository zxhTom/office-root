package com.github.zxhtom.ay;

import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.metadata.BaseRowModel;
import com.alibaba.excel.metadata.Sheet;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.github.zxhtom.ay.handler.StyleExcelHandler;
import org.junit.Test;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @package com.github.zxhtom.ay
 * @Class WriteTest
 * @Description 导出工具测试类
 * @Author zhangxinhua
 * @Date 19-9-23 下午1:54
 */
public class WriteTest {

    @Test
    public void exportStyle() throws IOException {
        StyleExcelHandler handler = new StyleExcelHandler();
        OutputStream outputStream = new FileOutputStream("/root/Downloads/test2.xlsx");
        // 这里要把上面创建的样式类通过构造函数传入
        ExcelWriter writer = new ExcelWriter(null, outputStream, ExcelTypeEnum.XLSX, true, handler);
        Sheet sheet1 = new Sheet(1, 1, Dangers.class, "1-生产作业（通用）", null);
        Sheet sheet2 = new Sheet(2, 1, Dangers.class, "2-生产作业（通用）", null);
        //设置列宽 设置每列的宽度
        Map columnWidth = new HashMap();
        /*columnWidth.put(0,10000);columnWidth.put(1,10000);columnWidth.put(2,10000);columnWidth.put(3,10000);
        sheet1.setColumnWidthMap(columnWidth);*/
        //或自适应宽度
        //sheet1.setAutoWidth(true);

        writer.write(data(), sheet1);
        writer.write(data(), sheet2);
        writer.finish();
        outputStream.close();
    }

    private List<? extends BaseRowModel> data() {
        List<Dangers> resultList = new ArrayList<>();
        Dangers dangers = new Dangers();
        dangers.setIndexNo("1");
        dangers.setDescription("油操、油检、锅炉、变配电、消防等倒班岗位作业准备");
        dangers.setDangers("倒班岗位夜班时间过长");
        dangers.setFenxianleibie("职业健康");
        dangers.setFenxianleibie("职业健康");
        dangers.setZhuyaohouguo("员工较难适应夜班12小时工作时间，造成心理疲劳，影响员工意识、反应速度，存在安全隐患");
        dangers.setAnquancuoshi("1、班前作业风险、安全注意事项宣贯、交接\n" +
                "2、根据人员配备，合理调配作业，轮流值班");
        dangers.setCaozuogangwei("操作岗位负责人");
        dangers.setKenengxing("1");
        dangers.setYanzhongxing("1");
        dangers.setFenxiandu("4");
        dangers.setFenxiandengji("III 级\n" +
                "一般风险");
        dangers.setJianyi("工会每年组织员工心理疏导等方面的讲座");
        resultList.add(dangers);
        return resultList;
    }
}

