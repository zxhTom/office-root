package com.github.zxhtom.temp;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;

/**
 * @package com.alibaba.easyexcel.test.demo.write
 * @Class tes
 * @Description TODO
 * @Author zhangxinhua
 * @Date 19-9-23 上午9:08
 */
public class BasePurchaseExecutionResponse extends BaseRowModel {

    /**
     * 序号
     */
    @ExcelProperty(value = {"", "", "序号"}, index = 0)
    private String num;
    /**
     * 供应商类型
     */
    @ExcelProperty(value = {"", "", "供应商类型"}, index = 1)
    private String supplierType;
    /**
     * 品牌
     */
    @ExcelProperty(value = {"", "", "品牌"}, index = 2)
    private String brandNameListString;

    /**
     * 年份
     */
    @ExcelProperty(value = {"", "", "年份"}, index = 3)
    private String productYear;
    /**
     * 产品季节
     */
    @ExcelProperty(value = {"", "", "产品季节"}, index = 4)
    private String productSeason;
    /**
     * 总量
     */
    @ExcelProperty(value = {"", "", "总量"}, index = 9)
    private int totalShipment;
    /**
     * 计划交期满足率
     */
    @ExcelProperty(value = {"", "", "计划交期满足率"}, index = 10)
    private String planDeliverRate;
    /**
     * 确认交期满足率
     */
    @ExcelProperty(value = {"", "", "确认交期满足率"}, index = 11)
    private String confirmDeliverRate;

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getSupplierType() {
        return supplierType;
    }

    public void setSupplierType(String supplierType) {
        this.supplierType = supplierType;
    }

    public String getBrandNameListString() {
        return brandNameListString;
    }

    public void setBrandNameListString(String brandNameListString) {
        this.brandNameListString = brandNameListString;
    }

    public String getProductYear() {
        return productYear;
    }

    public void setProductYear(String productYear) {
        this.productYear = productYear;
    }

    public String getProductSeason() {
        return productSeason;
    }

    public void setProductSeason(String productSeason) {
        this.productSeason = productSeason;
    }

    public int getTotalShipment() {
        return totalShipment;
    }

    public void setTotalShipment(int totalShipment) {
        this.totalShipment = totalShipment;
    }

    public String getPlanDeliverRate() {
        return planDeliverRate;
    }

    public void setPlanDeliverRate(String planDeliverRate) {
        this.planDeliverRate = planDeliverRate;
    }

    public String getConfirmDeliverRate() {
        return confirmDeliverRate;
    }

    public void setConfirmDeliverRate(String confirmDeliverRate) {
        this.confirmDeliverRate = confirmDeliverRate;
    }
}
