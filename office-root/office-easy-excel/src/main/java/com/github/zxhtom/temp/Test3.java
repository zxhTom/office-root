package com.github.zxhtom.temp;

import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.metadata.BaseRowModel;
import com.alibaba.excel.metadata.Sheet;
import com.alibaba.excel.support.ExcelTypeEnum;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @package com.alibaba.easyexcel.test.demo.write
 * @Class Test
 * @Description easyExcel不足之处：
 *        样式的设置可维护性太差，通过在样式类中硬编码各个列号对应的样式确实不太好，问题点在于，单元格样式的创建需要
 *        workBook对象实例才可以，框架本身并没有提供获取wirkBook的方法，因此，如果想要在其他地方设置样式的话，可以
 *        通过反射的方式来获取workbook对象
 * @Author zhangxinhua
 * @Date 19-9-23 上午9:10
 */
public class Test3 {

    public static void main(String[] args) throws IOException {
        StyleExcelHandler handler = new StyleExcelHandler();
        OutputStream outputStream = new FileOutputStream("/root/Downloads/test.xlsx");
        // 这里要把上面创建的样式类通过构造函数传入
        ExcelWriter writer = new ExcelWriter(null, outputStream, ExcelTypeEnum.XLSX, true, handler);
        Sheet sheet1 = new Sheet(1, 1, BasePurchaseExecutionResponse.class, "含供应商和地区", null);
        //设置列宽 设置每列的宽度
        Map columnWidth = new HashMap();
        columnWidth.put(0,10000);columnWidth.put(1,10000);columnWidth.put(2,10000);columnWidth.put(3,10000);
        sheet1.setColumnWidthMap(columnWidth);
        //或自适应宽度
        //sheet1.setAutoWidth(true);

        writer.write(createResponseList(), sheet1);
        writer.finish();
        outputStream.close();
    }


    /**
     * 创建数据集合
     *
     * @return
     */
    private static List<? extends BaseRowModel> createResponseList() {
        List<BasePurchaseExecutionResponse> responses = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            BasePurchaseExecutionResponse response = new BasePurchaseExecutionResponse();
            response.setTotalShipment(i * 1000000);
            response.setConfirmDeliverRate( i+ "%");
            response.setNum(String.valueOf(i));
            response.setProductSeason("冬收到甲方");
            response.setProductYear("19");
            response.setSupplierType("本厂");
            response.setBrandNameListString("耐特");
            response.setPlanDeliverRate(i * 2 + "%");
            responses.add(response);
        }
        return responses;
    }
}
