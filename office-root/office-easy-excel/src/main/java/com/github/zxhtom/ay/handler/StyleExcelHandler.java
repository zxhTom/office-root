package com.github.zxhtom.ay.handler;

import com.alibaba.excel.event.WriteHandler;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

/**
 * @package com.github.zxhtom.ay.handler
 * @Class StyleExcelHandler
 * @Description TODO
 * @Author zhangxinhua
 * @Date 19-9-23 下午1:53
 */
public class StyleExcelHandler implements WriteHandler {
    @Override
    public void sheet(int i, Sheet sheet) {

    }

    @Override
    public void row(int i, Row row) {
        System.out.println(row);
        if (i == 1) {
            //System.out.println(row.getCell(0).getStringCellValue());
        }
    }

    @Override
    public void cell(int i, Cell cell) {
        System.out.println(cell);
    }
}
