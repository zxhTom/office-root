package com.github.zxhtom.ay;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;

/**
 * @package com.github.zxhtom.ay
 * @Class Dangers
 * @Description TODO
 * @Author zhangxinhua
 * @Date 19-9-23 上午9:25
 */
@Data
public class Dangers extends BaseRowModel {

    @ExcelProperty(value = {Constant.FIRST,Constant.SECOND,Constant.THIRD,"序号"})
    private String indexNo;
    @ExcelProperty(value = {Constant.FIRST,Constant.SECOND,Constant.THIRD,"工作步骤描述"})
    private String description;
    @ExcelProperty(value = {Constant.FIRST,Constant.SECOND,Constant.THIRD,"危险有害因素"})
    private String dangers;
    @ExcelProperty(value = {Constant.FIRST,Constant.SECOND,Constant.THIRD,"类型"})
    private String fenxianleibie;
    @ExcelProperty(value = {Constant.FIRST,Constant.SECOND,Constant.THIRD,"主要后果"})
    private String zhuyaohouguo;
    @ExcelProperty(value = {Constant.FIRST,Constant.SECOND,Constant.THIRD,"现有安全控制措施"})
    private String anquancuoshi;
    @ExcelProperty(value = {Constant.FIRST,Constant.SECOND,Constant.THIRD,"操作岗位"})
    private String caozuogangwei;
    @ExcelProperty(value = {Constant.FIRST,Constant.SECOND,Constant.THIRD,"危害可能性"})
    private String kenengxing;
    @ExcelProperty(value = {Constant.FIRST,Constant.SECOND,Constant.THIRD,"后果严重性"})
    private String yanzhongxing;
    @ExcelProperty(value = {Constant.FIRST,Constant.SECOND,Constant.THIRD,"风险度"})
    private String fenxiandu;
    @ExcelProperty(value = {Constant.FIRST,Constant.SECOND,Constant.THIRD,"风险等级"})
    private String fenxiandengji;
    @ExcelProperty(value = {Constant.FIRST,Constant.SECOND,Constant.THIRD,"建议"})
    private String jianyi;


}
